/**LIBRARY::Utility**/
/* Watchers to keep blank dropdowns from appearing */
{!if DOCUMENT.ALMOND_TYPE=="" then DOCUMENT.ALMOND_TYPE="Almond"endif}
{!if DOCUMENT.BRAZIL_NUT_TYPE=="" then DOCUMENT.BRAZIL_NUT_TYPE="Brazil Nut"endif}
{!if DOCUMENT.CASHEW_TYPE=="" then DOCUMENT.CASHEW_TYPE="Cashew"endif}
{!if DOCUMENT.HAZELNUT_TYPE=="" then DOCUMENT.HAZELNUT_TYPE="Hazelnut"endif}
{!if DOCUMENT.FARR_PEANUT_TYPE=="" then DOCUMENT.FARR_PEANUT_TYPE="Peanut (Bamba)"endif}
{!if DOCUMENT.PEANUT_TYPE=="" then DOCUMENT.PEANUT_TYPE="Peanut"endif}
{!if DOCUMENT.PISTACHIO_TYPE=="" then DOCUMENT.PISTACHIO_TYPE="Pistachio"endif}
{!if DOCUMENT.SESAME_TYPE=="" then DOCUMENT.SESAME_TYPE="Sesame"endif}
{!if DOCUMENT.WALNUT_TYPE=="" then DOCUMENT.WALNUT_TYPE="Walnut"endif}
{!global vis=if ok(DOCUMENT.OVERRIDE_RB) and DOCUMENT.OVERRIDE_RB<>"" then DOCUMENT.OVERRIDE_RB else if PATIENT_AGE()<4 then "<4 y/o" else "4+ y/o" endif endif}
/* Watchers to build list of reactions */
{DOCUMENT.REACTIONS=fnFoodChallengeRecordReactions(DOCUMENT.NO_PREVIOUS_EXPOSURE,DOCUMENT.ANAPHYLAXIS,DOCUMENT.RESP_SX,DOCUMENT.HIVES,DOCUMENT.OTHER_RASH,DOCUMENT.WHEEZE,DOCUMENT.VOMITING,DOCUMENT.DIARRHEA,DOCUMENT.OTHER_REACTIONS)}
{DOCUMENT.REACTIONS4=fnFoodChallengeRecordReactions(DOCUMENT.NO_PREVIOUS_EXPOSURE4,DOCUMENT.ANAPHYLAXIS4,DOCUMENT.RESP_SX4,DOCUMENT.HIVES4,DOCUMENT.OTHER_RASH4,DOCUMENT.WHEEZE4,DOCUMENT.VOMITING4,DOCUMENT.DIARRHEA4,DOCUMENT.OTHER_REACTIONS4)}
{DOCUMENT.REACTIONS1=fnFoodChallengeRecordReactions(DOCUMENT.NO_PREVIOUS_EXPOSURE1,DOCUMENT.ANAPHYLAXIS1,DOCUMENT.RESP_SX1,DOCUMENT.HIVES1,DOCUMENT.OTHER_RASH1,DOCUMENT.WHEEZE1,DOCUMENT.VOMITING1,DOCUMENT.DIARRHEA1,DOCUMENT.OTHER_REACTIONS1)}
{DOCUMENT.REACTIONS2=fnFoodChallengeRecordReactions(DOCUMENT.NO_PREVIOUS_EXPOSURE2,DOCUMENT.ANAPHYLAXIS2,DOCUMENT.RESP_SX2,DOCUMENT.HIVES2,DOCUMENT.OTHER_RASH2,DOCUMENT.WHEEZE2,DOCUMENT.VOMITING2,DOCUMENT.DIARRHEA2,DOCUMENT.OTHER_REACTIONS2)}

fn fnFoodChallengeRecordReactions()
{
	local retStr=""
	local i,thisOne,n=getnargs()
	for i=1,i<=n,i=i+1
	do
		thisOne=getarg(i)
		if (thisOne=="no previous exposure") then
			retStr=thisOne
			break
		else
			retStr=fnUtilityAppendList(retStr,thisOne)
		endif
	endfor
	return retStr
}

fn fnFoodChallengeRecordXlateInterval(dd)
{
	local retStr=dd
	if (retStr=="baked challenge") then
		retStr="15 minutes between 1/4 servings of sponge cake, then 30 minute wait after full sponge cake.  15 minutes between ¼ servings of pancake/waffle"
	endif
	return retStr
}

/*******************************************************************/
/* Function: fnFoodChallengeAlmond()
/* Purpose: Fill in fields based on almond selection and button clicked.
/*******************************************************************/
fn fnFoodChallengeAlmond(food,age,stdExt)
{
	local doses="",challengeFood=""
	// Load the doses based on food type, age range, and whether user clicked Standard or Extended 
	if food=="Almond" then
		challengeFood="almond milk/almonds"
		cond
			case age=="<4" and stdExt=="std"
				doses="3 mL^almond milk|0.5 g^almond|4^almonds|12^almonds|^|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="3 mL^almond milk|0.5 g^almond|4^almonds|20^almonds|^|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="0.5 mL^almond milk|2 mL^almond milk|0.2 g^almond|0.5 g^almond|1^almond|4^almonds|12^almonds|^"
			case age=="4+" and stdExt=="ext"
				doses="0.5 mL^almond milk|2 mL^almond milk|0.2 g^almond|0.5 g^almond|1^almond|4^almonds|20^almonds|^"
		endcond
	endif
	if food=="Almond Milk" then
		challengeFood="almond milk/almonds"
		cond
			case age=="<4" and stdExt=="std"
				doses="3 mL^almond milk|25 mL^almond milk|180 mL^almond milk|15 g^almond butter|^|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="3 mL^almond milk|25 mL^almond milk|180 mL^almond milk|20^almonds|^|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="0.5 mL^almond milk|2 mL^almond milk|10 mL^almond milk|25 mL^almond milk|60 mL^almond milk|120 mL^almond milk|15 g^almond butter|^"
			case age=="4+" and stdExt=="ext"
				doses="0.5 mL^almond milk|2 mL^almond milk|10 mL^almond milk|25 mL^almond milk|60 mL^almond milk|120 mL^almond milk|20^almonds|^"
		endcond
	endif
	if food=="Almond Butter" then
		challengeFood="almond milk/almond butter"
		cond
			case age=="<4" and stdExt=="std"
				doses="3 mL^almond milk|0.5 g^almond butter|3.75 g^almond butter|15 g^almond butter|^|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="3 mL^almond milk|0.5 g^almond butter|3.75 g^almond butter|25 g^almond butter|^|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="0.5 mL^almond milk|2 mL^almond milk|0.2 g^almond butter|0.5 g^almond butter|1.2 g^almond butter|2.5 g^almond butter|15 g^almond butter|^"
			case age=="4+" and stdExt=="ext"
				doses="0.5 mL^almond milk|2 mL^almond milk|0.2 g^almond butter|0.5 g^almond butter|1.2 g^almond butter|2.5 g^almond butter|25 g^almond butter|^"
		endcond
	endif

	if doses=="" then
		userok("Can't find the dose")
		return ""
	endif
	DOCUMENT.FOODALLERGEN="almond"
	DOCUMENT.CHALLENGE_FOOD=challengeFood
	//Call function below to update fields on screen:
	fnFoodChallengeUpdateFieldsOnScreen("",doses)
	return ""
}
/*******************************************************************/
/* Function: fnFoodChallengeBrazilNut()
/* Purpose: Fill in fields based on Brazil nut selection and button clicked.
/*******************************************************************/
fn fnFoodChallengeBrazilNut(food,age,stdExt)
{
	local doses="",challengeFood=""
	// Load the doses based on food type, age range, and whether user clicked Standard or Extended 
	if food=="Brazil Nut" then
		challengeFood="Brazil nut flour solution/Brazil nut"
		cond
			case age=="<4" and stdExt=="std"
				doses="2.5 mL^30 mg/mL Brazil nut flour solution|0.7 g^Brazil nut|7 g^Brazil nut|14 g^Brazil nut|^|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="2.5 mL^30 mg/mL Brazil nut flour solution|0.7 g^Brazil nut|7 g^Brazil nut|14 g^Brazil nut|17.5 g^Brazil nut|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="8 mL^2 mg/mL Brazil nut flour solution|2.5 mL^30 mg/mL Brazil nut flour solution|0.3 g^Brazil nut|0.7 g^Brazil nut|1.8 g^Brazil nut|7 g^Brazil nut|10.5 g^Brazil nut|^"
			case age=="4+" and stdExt=="ext"
				doses="8 mL^2 mg/mL Brazil nut flour solution|2.5 mL^30 mg/mL Brazil nut flour solution|0.3 g^Brazil nut|0.7 g^Brazil nut|1.8 g^Brazil nut|14 g^Brazil nut|21 g^Brazil nut|^"
		endcond
	endif
	if food=="Brazil Nut Butter" then
		challengeFood="Brazil nut flour solution/Brazil nut butter"
		cond
			case age=="<4" and stdExt=="std"
				doses="2.5 mL^30 mg/mL Brazil nut flour solution|0.7 g^Brazil nut butter|7 g^Brazil nut butter|14 g^Brazil nut butter|^|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="2.5 mL^30 mg/mL Brazil nut flour solution|0.7 g^Brazil nut butter|7 g^Brazil nut butter|14 g^Brazil nut butter|17.5 g^Brazil nut butter|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="8 mL^2 mg/mL Brazil nut flour solution|2.5 mL^30 mg/mL Brazil nut flour solution|0.3 g^Brazil nut butter|0.7 g^Brazil nut butter|1.8 g^Brazil nut butter|7 g^Brazil nut butter|10.5 g^Brazil nut butter|^"
			case age=="4+" and stdExt=="ext"
				doses="8 mL^2 mg/mL Brazil nut flour solution|2.5 mL^30 mg/mL Brazil nut flour solution|0.3 g^Brazil nut butter|0.7 g^Brazil nut butter|1.8 g^Brazil nut butter|14 g^Brazil nut butter|21 g^Brazil nut butter|^"
		endcond
	endif
	if doses=="" then
		userok("Can't find the dose")
		return ""
	endif
	DOCUMENT.FOODALLERGEN="Brazil nut"
	DOCUMENT.CHALLENGE_FOOD=challengeFood
	//Call function below to update fields on screen:
	fnFoodChallengeUpdateFieldsOnScreen("",doses)
	return ""
}
/*******************************************************************/
/* Function: fnFoodChallengeCashew()
/* Purpose: Fill in fields based on cashew selection and button clicked.
/*******************************************************************/
fn fnFoodChallengeCashew(food,age,stdExt)
{
	local doses="",challengeFood=""
	// Load the doses based on food type, age range, and whether user clicked Standard or Extended 
	if food=="Cashew" then
		challengeFood="cashew/cashew flour solution"
		cond
			case age=="<4" and stdExt=="std"
				doses="1 mL^5 mg/mL solution|9 mL^5 mg/mL solution|0.2 g^cashew|4^cashews (5.6 g)|12^cashews (16.7 g)|^|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="1 mL^5 mg/mL solution|9 mL^5 mg/mL solution|0.2 g^cashew|2^cashews (2.8 g)|22^cashews (30.6 g)|^|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="1 mL^5 mg/mL solution|9 mL^5 mg/mL solution|0.2 g^cashew|2^cashews (2.8 g)|6^cashews (8.3 g)|8^cashews (11.1 g)|^|^"
			case age=="4+" and stdExt=="ext"
				doses="1 mL^5 mg/mL solution|9 mL^5 mg/mL solution|0.2 g^cashew|1^cashew (1.4 g)|8^cashews (11.1 g)|15^cashews (21 g)|^|^"
		endcond
	endif
	if food=="Cashew Flour" then
		challengeFood="cashew flour solution/cashew flour"
		cond
			case age=="<4" and stdExt=="std"
				doses="1 mL^5 mg/mL solution|9 mL^5 mg/mL solution|202 mg^cashew flour|5600 mg^cashew flour|16800 mg^cashew flour|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="1 mL^5 mg/mL solution|9 mL^5 mg/mL solution|202 mg^cashew flour|2800 mg^cashew flour|30800 mg^cashew flour|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="1 mL^5 mg/mL solution|9 mL^5 mg/mL solution|202 mg^cashew flour|2800 mg^cashew flour|8400 mg^cashew flour|11200 mg^cashew flour|^|^"
			case age=="4+" and stdExt=="ext"
				doses="1 mL^5 mg/mL solution|9 mL^5 mg/mL solution|202 mg^cashew flour|1400 mg^cashew flour|11200 mg^cashew flour|21000 mg^cashew flour|^|^"
		endcond
	endif
	if food=="Cashew Butter" then
		challengeFood="cashew flour solution/cashew butter/cashews"
		cond
			case age=="<4" and stdExt=="std"
				doses="1 mL^5 mg/mL solution|9 mL^5 mg/mL solution|0.2 g^cashew|1 tsp^cashew butter|3 tsp^cashew butter|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="1 mL^5 mg/mL solution|9 mL^5 mg/mL solution|0.2 g^cashew|1/2 tsp^cashew butter|5 1/2 tsp^cashew butter|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="1 mL^5 mg/mL solution|9 mL^5 mg/mL solution|0.2 g^cashew|1/2 tsp^cashew butter|1 1/2 tsp^cashew butter|2 tsp^cashew butter|^|^"
			case age=="4+" and stdExt=="ext"
				doses="1 mL^5 mg/mL solution|9 mL^5 mg/mL solution|0.2 g^cashew|1/4 tsp^cashew butter|2 tsp^cashew butter|3 3/4 tsp^cashew butter|^|^"
		endcond
	endif
	if food=="Cashew Milk" then
		challengeFood="cashew flour solution/cashew milk"
		cond
			case age=="<4" and stdExt=="std"
				doses="1 mL^5 mg/mL solution|0.5 mL^cashew milk|2 mL^cashew milk|60 mL^cashew milk|180 mL^cashew milk|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="1 mL^5 mg/mL solution|0.5 mL^cashew milk|2 mL^cashew milk|30 mL^cashew milk|330 mL^cashew milk|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="1 mL^5 mg/mL solution|0.5 mL^cashew milk|2 mL^cashew milk|30 mL^cashew milk|90 mL^cashew milk|120 mL^cashew milk|^|^"
			case age=="4+" and stdExt=="ext"
				doses="1 mL^5 mg/mL solution|0.5 mL^cashew milk|2 mL^cashew milk|15 mL^cashew milk|120 mL^cashew milk|225 mL^cashew milk|^|^"
		endcond
	endif
	if doses=="" then
		userok("Can't find the dose")
		return ""
	endif
	DOCUMENT.FOODALLERGEN="cashew"
	DOCUMENT.CHALLENGE_FOOD=challengeFood
	//Call function below to update fields on screen:
	fnFoodChallengeUpdateFieldsOnScreen("",doses)
	return ""
}

/*******************************************************************/
/* Function: fnFoodChallengeFarrEgg()
/* Purpose: Fill in fields based on the FARR Egg button clicked.
/*******************************************************************/
fn fnFoodChallengeFarrEgg(stdExt)
{
	local doses=""
	if stdExt=="std" then
		doses="0.2 mL^liquid egg white|1 mL^liquid egg white|9 mL^liquid egg white |16 mL^liquid egg white |^|^|^|^"
	else
		doses="3 mL^1 mg/mL solution|0.2 mL^liquid egg white |1 mL^liquid egg white |2.5 mL^liquid egg white |6 mL^liquid egg white |16 mL^liquid egg white |^|^"
	endif
	DOCUMENT.FOODALLERGEN1="egg"
	DOCUMENT.CHALLENGE_FOOD1="egg white powder/liquid egg white"
	//Call function below to update fields on screen:
	fnFoodChallengeUpdateFieldsOnScreen("1",doses)
	return ""
}
/*******************************************************************/
/* Function: fnFoodChallengeEgg()
/* Purpose: Fill in fields based on the Egg button clicked.
/*******************************************************************/
fn fnFoodChallengeEgg(age,stdExt)
{
	local doses="",challengeFood=""
	cond
		case age=="<4" and stdExt=="std"
			doses="0.2 mL^liquid egg white|1 mL^liquid egg white|9 mL^liquid egg white|33 mL^liquid egg white|^|^|^|^"
			challengeFood="liquid egg white"
		case age=="4+" and stdExt=="std"
			doses="0.2 mL^liquid egg white|1 mL^liquid egg white|9 mL^liquid egg white|39 mL^liquid egg white|^|^|^|^"
			challengeFood="liquid egg white"
		case age=="<4" and stdExt=="ext"
			doses="3 mL^1 mg/mL solution|0.2 mL^liquid egg white|1 mL^liquid egg white|4 mL^liquid egg white|11.25 mL^liquid egg white |27 mL^liquid egg white |^|^"
			challengeFood="liquid egg white solution/liquid egg white"
		case age=="4+" and stdExt=="ext"
			doses="3 mL^1 mg/mL solution|0.2 mL^liquid egg white|1 mL^liquid egg white|2.5 mL^liquid egg white|11.25 mL^liquid egg white |33 mL^liquid egg white |^|^"
			challengeFood="liquid egg white solution/liquid egg white"
	endcond
	DOCUMENT.FOODALLERGEN="egg"
	DOCUMENT.CHALLENGE_FOOD=challengeFood
	//Call function below to update fields on screen:
	fnFoodChallengeUpdateFieldsOnScreen("",doses)
	return ""
}
/*******************************************************************/
/* Function: fnFoodChallengeHazelnut()
/* Purpose: Fill in fields based on hazelnut selection and button clicked.
/*******************************************************************/
fn fnFoodChallengeHazelnut(food,age,stdExt)
{
	local doses="",challengeFood=""
	// Load the doses based on food type, age range, and whether user clicked Standard or Extended 
	if food=="Hazelnut" then
		challengeFood="hazelnut flour solution/hazelnuts"
		cond
			case age=="<4" and stdExt=="std"
				doses="4 mL^15 mg/mL solution|0.4 g^hazelnut|4^hazelnuts (5.6 g)|10^hazelnuts (14 g)|^|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="4 mL^15 mg/mL solution|0.4 g^hazelnut|4^hazelnuts (5.6 g)|12^hazelnuts (16.8 g)|^|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="1 mL^15 mg/mL solution|4 mL^15 mg/mL solution|0.2 g^hazelnut|0.8 g^hazelnut|1^hazelnut (1.4 g)|4^hazelnuts (5.6 g)|8^hazelnuts (11.2 g)|^"
			case age=="4+" and stdExt=="ext"
				doses="1 mL^15 mg/mL solution|4 mL^15 mg/mL solution|0.2 g^hazelnut|0.8 g^hazelnut|1^hazelnut (1.4 g)|4^hazelnuts (5.6 g)|10^hazelnuts (14 g)|^"
		endcond
	endif
	if food=="Hazelnut Milk" then
		challengeFood="hazelnut milk"
		cond
			case age=="<4" and stdExt=="std"
				doses="0.6 mL^hazelnut milk|4 mL^hazelnut milk|48 mL^hazelnut milk|120 mL^hazelnut milk|^|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="0.6 mL^hazelnut milk|4 mL^hazelnut milk|48 mL^hazelnut milk|144 mL^hazelnut milk|^|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="0.1 mL^hazelnut milk|0.6 mL^hazelnut milk|2 mL^hazelnut milk|8 mL^hazelnut milk|12 mL^hazelnut milk|48 mL^hazelnut milk|96 mL^hazelnut milk|^"
			case age=="4+" and stdExt=="ext"
				doses="0.1 mL^hazelnut milk|0.6 mL^hazelnut milk|2 mL^hazelnut milk|8 mL^hazelnut milk|12 mL^hazelnut milk|48 mL^hazelnut milk|120 mL^hazelnut milk|^"
		endcond
	endif
	if food=="Hazelnut Butter" then
		challengeFood="hazelnut flour solution/hazelnut butter"
		cond
			case age=="<4" and stdExt=="std"
				doses="4 mL^15 mg/mL solution|0.4 g^hazelnut butter|5 g^hazelnut butter|12.5 g^hazelnut butter|^|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="4 mL^15 mg/mL solution|0.4 g^hazelnut butter|5.6 g^hazelnut butter|15 g^hazelnut butter|^|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="1 mL^15 mg/mL solution|4 mL^15 mg/mL solution|0.2 g^hazelnut butter|0.8 g^hazelnut butter|1.2 g^hazelnut butter|5 g^hazelnut butter|10 g^hazelnut butter|^"
			case age=="4+" and stdExt=="ext"
				doses="1 mL^15 mg/mL solution|4 mL^15 mg/mL solution|0.2 g^hazelnut butter|0.8 g^hazelnut butter|1.2 g^hazelnut butter|5 g^hazelnut butter|12.5 g^hazelnut butter|^"
		endcond
	endif
	if doses=="" then
		userok("Can't find the dose")
		return ""
	endif
	DOCUMENT.FOODALLERGEN="hazelnut"
	DOCUMENT.CHALLENGE_FOOD=challengeFood
	//Call function below to update fields on screen:
	fnFoodChallengeUpdateFieldsOnScreen("",doses)
	return ""
}
/*******************************************************************/
/* Function: fnFoodChallengeMacadamia()
/* Purpose: Fill in fields based on the Macadamia button clicked.
/*******************************************************************/
fn fnFoodChallengeMacadamia(age,stdExt)
{
	local doses=""
	cond
		case age=="<4" and stdExt=="std"
			doses="4 mL^30 mg/mL macadamia flour solution|1 g^macadamia|2 (5.2 g)^macadamia nuts|6 (15.6 g)^macadamia nuts|^|^|^|^"
		case age=="4+" and stdExt=="std"
			doses="4 mL^30 mg/mL macadamia flour solution|1 g^macadamia|2 (5.2 g)^macadamia nuts|10 (26 g)^macadamia nuts|^|^|^|^"
		case age=="<4" and stdExt=="ext"
			doses="1.5 mL^30 mg/mL macadamia flour solution|4 mL^30 mg/mL macadamia flour solution|0.6 g^macadamia|1.4 g^macadamia|1 (2.6 g)^macadamia nut|2 (5.2 g)^macadamia nuts|4 (10.4 g)^macadamia nuts|^"
		case age=="4+" and stdExt=="ext"
			doses="1.5 mL^30 mg/mL macadamia flour solution|4 mL^30 mg/mL macadamia flour solution|0.6 g^macadamia|1 g^macadamia|1 (2.6 g)^macadamia nut|2 (5.2 g)^macadamia nuts|8 (20.8 g)^macadamia nuts|^"
	endcond
	DOCUMENT.FOODALLERGEN="macadamia"
	DOCUMENT.CHALLENGE_FOOD="macadamia nuts/macadamia flour solution"
	//Call function below to update fields on screen:
	fnFoodChallengeUpdateFieldsOnScreen("",doses)
	return ""
}
/*******************************************************************/
/* Function: fnFoodChallengeFARRMilk()
/* Purpose: Fill in fields based on the FARR Milk button clicked.
/*******************************************************************/
fn fnFoodChallengeFARRMilk(stdExt)
{
	local doses=""
	if stdExt=="std" then
		doses="0.3 mL^milk|3 mL^milk|30 mL^milk|90 mL^milk|^|^|^|^"
	else
		doses="0.3 mL^milk|3 mL^milk|20 mL^milk|45 mL^milk|60 mL^milk|^|^|^"
	endif
	DOCUMENT.FOODALLERGEN1="milk"
	DOCUMENT.CHALLENGE_FOOD1="milk"
	//Call function below to update fields on screen:
	fnFoodChallengeUpdateFieldsOnScreen("1",doses)
	return ""
}
/*******************************************************************/
/* Function: fnFoodChallengeMilk()
/* Purpose: Fill in fields based on the Milk button clicked.
/*******************************************************************/
fn fnFoodChallengeMilk(age,stdExt)
{
	local doses=""
	cond
		case age=="<4" and stdExt=="std"
			doses="0.3 mL^whole milk|3 mL^whole milk|60 mL^whole milk |120 mL^whole milk |^|^|^|^"
		case age=="4+" and stdExt=="std"
			doses="0.3 mL^whole milk|3 mL^whole milk|20 mL^whole milk |220 mL^whole milk |^|^|^|^"
		case age=="<4" and stdExt=="ext"
			doses="2 mL^1 mg/mL solution|6 mL^1 mg/mL solution|0.3 mL^whole milk|1 mL^whole milk|3 mL^whole milk|20 mL^whole milk|75 mL^whole milk|90 mL^whole milk"
		case age=="4+" and stdExt=="ext"
			doses="2 mL^1 mg/mL solution|6 mL^1 mg/mL solution|0.3 mL^whole milk|1 mL^whole milk|3 mL^whole milk|10 mL^whole milk|60 mL^whole milk|165 mL^whole milk"
	endcond
	DOCUMENT.FOODALLERGEN="milk"
	DOCUMENT.CHALLENGE_FOOD="whole milk"
	//Call function below to update fields on screen:
	fnFoodChallengeUpdateFieldsOnScreen("",doses)
	return ""
}
/*******************************************************************/
/* Function: fnFoodChallengeFARRPeanut()
/* Purpose: Fill in fields based on the FARR Peanut button clicked.
/*******************************************************************/
fn fnFoodChallengeFARRPeanut(food,stdExt)
{
	local doses="",challengeFood=""
	// Load the doses based on food type,and whether user clicked Standard or Extended 
	if food=="Peanut (Bamba)" then
		challengeFood="peanut flour/Bamba"
		if stdExt=="std" then
			doses="1 mL^25 mg/mL solution|1.3 g^Bamba|6.2 g^Bamba|8.3 g^Bamba|^|^|^|^"
		else
			doses="2 mL^2.5 mg/mL solution|1 mL^25 mg/mL solution|0.2 g^Bamba|1.3 g^Bamba|6.2 g^Bamba|8.3 g^Bamba|^|^"
		endif
	endif
	if food=="Peanut (TruNut)" then
		challengeFood="peanut flour/TruNut"
		if stdExt=="std" then
			doses="1 mL^25 mg/mL solution|0.4 g^TruNut|1.8 g^TruNut|2.4 g^TruNut|^|^|^|^"
		else
			doses="2 mL^2.5 mg/mL solution|1 mL^25 mg/mL solution|0.1 g^TruNut|0.4 g^TruNut|1.8 g^TruNut|2.4 g^TruNut|^|^"
		endif
	endif
	if doses=="" then
		userok("Can't find the dose")
		return ""
	endif
	DOCUMENT.FOODALLERGEN1="peanut"
	DOCUMENT.CHALLENGE_FOOD1=challengeFood
	//Call function below to update fields on screen:
	fnFoodChallengeUpdateFieldsOnScreen("1",doses)
	return ""
""
}
/*******************************************************************/
/* Function: fnFoodChallengePeanut()
/* Purpose: Fill in fields based on peanut selection and button clicked.
/*******************************************************************/
fn fnFoodChallengePeanut(food,age,stdExt)
{
	local doses="",challengeFood=""
	// Load the doses based on food type, age range, and whether user clicked Standard or Extended 
	if food=="Peanut" then
		challengeFood="peanut flour solution/peanuts"
		cond
			case age=="<4" and stdExt=="std"
				doses="1 mL^25 mg/mL solution|0.4 g^peanut|4^peanuts|12^peanuts|^|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="1 mL^25 mg/mL solution|0.4 g^peanut|4^peanuts|20^peanuts|^|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="2 mL^2.5 mg/mL solution|1 mL^25 mg/mL solution|0.2 g^peanut|0.4 g^peanut|1^peanuts|4^peanuts|12^peanuts|^"
			case age=="4+" and stdExt=="ext"
				doses="2 mL^2.5 mg/mL solution|1 mL^25 mg/mL solution|0.2 g^peanut|0.4 g^peanut|1^peanuts|3^peanuts|20^peanuts|^"
		endcond
	endif
	if food=="TruNut" then
		challengeFood="peanut flour solution/TruNut"
		cond
			case age=="<4" and stdExt=="std"
				doses="1 mL^25 mg/mL solution|0.3 g^TruNut|2.4 g^TruNut|7.2 g^TruNut|^|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="1 mL^25 mg/mL solution|0.3 g^TruNut|2.4 g^TruNut|12 g^TruNut|^|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="2 mL^2.5 mg/mL solution|1 mL^25 mg/mL solution|0.1 g^TruNut|0.3 g^TruNut|0.6 g^TruNut|2.4 g^TruNut|7.2 g^TruNut|^"
			case age=="4+" and stdExt=="ext"
				doses="2 mL^2.5 mg/mL solution|1 mL^25 mg/mL solution|0.1 g^TruNut|0.3 g^TruNut|0.6 g^TruNut|1.8 g^TruNut|12 g^TruNut|^"
		endcond
	endif
	if food=="Bamba" then
		challengeFood="peanut flour solution/Bamba"
		cond
			case age=="<4" and stdExt=="std"
				doses="1 mL^25 mg/mL solution|0.9 g^Bamba|8.3 g^Bamba|25 g^Bamba|^|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="1 mL^25 mg/mL solution|0.9 g^Bamba|8.3 g^Bamba|42 g^Bamba|^|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="2 mL^2.5 mg/mL solution|1 mL^25 mg/mL solution|0.4 g^Bamba|0.9 g^Bamba|2.1 g^Bamba|8.3 g^Bamba|25 g^Bamba|^"
			case age=="4+" and stdExt=="ext"
				doses="2 mL^2.5 mg/mL solution|1 mL^25 mg/mL solution|0.4 g^Bamba|0.9 g^Bamba|2.1 g^Bamba|8.3 g^Bamba|42 g^Bamba|^"
		endcond
	endif
	if food=="Peanut Butter" then
		challengeFood="peanut flour solution/peanut butter/peanuts"
		cond
			case age=="<4" and stdExt=="std"
				doses="1 mL^25 mg/mL solution|0.4 g^peanut|1 tsp^peanut butter|3 tsp^peanut butter|^|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="1 mL^25 mg/mL solution|0.4 g^peanut|1 tsp^peanut butter|5 tsp^peanut butter|^|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="2 mL^2.5 mg/mL solution|1 mL^25 mg/mL solution|0.2 g^peanut|0.4 g^peanut|1/4 tsp^peanut butter|1 tsp^peanut butter|3 tsp^peanut butter|^"
			case age=="4+" and stdExt=="ext"
				doses="2 mL^2.5 mg/mL solution|1 mL^25 mg/mL solution|0.2 g^peanut|0.4 g^peanut|1/4 tsp^peanut butter|3/4 tsp^peanut butter|5 tsp^peanut butter|^"
		endcond
	endif
	if food=="Reese's Pieces" then
		challengeFood="peanut flour solution/Reese's Pieces"
		cond
			case age=="<4" and stdExt=="std"
				doses="1 mL^25 mg/mL solution|2^Reeses Pieces|22^Reeses Pieces|65^Reeses Pieces|^|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="1 mL^25 mg/mL solution|2^Reeses Pieces|22^Reeses Pieces|108^Reeses Pieces|^|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="2 mL^2.5 mg/mL solution|1 mL^25 mg/mL solution|1^Reeses Pieces|2^Reeses Pieces|5^Reeses Pieces|22^Reeses Pieces|65^Reeses Pieces|^"
				challengeFood="peanut flour solution/Reeses Pieces/peanut"
			case age=="4+" and stdExt=="ext"
				doses="2 mL^2.5 mg/mL solution|1 mL^25 mg/mL solution|1^Reeses Pieces|2^Reeses Pieces|5^Reeses Pieces|22^Reeses Pieces|108^Reeses Pieces|^"
				challengeFood="peanut flour solution/Reeses Pieces/peanut"
		endcond
	endif
	if food=="Peanut M&Ms" then
		challengeFood="peanut flour solution/Peanut M&Ms/peanuts"
		cond
			case age=="<4" and stdExt=="std"
				doses="1 mL^25 mg/mL solution|0.4 g^peanut|6^Peanut M&Ms|20^Peanut M&Ms|^|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="1 mL^25 mg/mL solution|0.4 g^peanut|6^Peanut M&Ms|34^Peanut M&Ms|^|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="2 mL^2.5 mg/mL solution|1 mL^25 mg/mL solution|0.2 g^peanut|0.4 g^peanut|2^Peanut M&Ms|6^Peanut M&Ms|20^Peanut M&Ms|^"
			case age=="4+" and stdExt=="ext"
				doses="2 mL^2.5 mg/mL solution|1 mL^25 mg/mL solution|0.2 g^peanut|0.4 g^peanut|2^Peanut M&Ms|6^Peanut M&Ms|34^Peanut M&Ms|^"
		endcond
	endif
	if doses=="" then
		userok("Can't find the dose")
		return ""
	endif
	DOCUMENT.FOODALLERGEN="peanut"
	DOCUMENT.CHALLENGE_FOOD=challengeFood
	//Call function below to update fields on screen:
	fnFoodChallengeUpdateFieldsOnScreen("",doses)
	return ""
}
/*******************************************************************/
/* Function: fnFoodChallengePecan()
/* Purpose: Fill in fields based on the Pecan button clicked.
/*******************************************************************/
fn fnFoodChallengePecan(age,stdExt)
{
	local doses="",challengeFood="pecan solution/pecan"
	cond
		case age=="<4" and stdExt=="std"
			doses="1 mL^10 mg/mL solution|2.5 mL^10 mg/mL solution|0.3 g^pecan|1^pecan|4^pecans|^|^|^"
		case age=="4+" and stdExt=="std"
			doses="1 mL^10 mg/mL solution|2.5 mL^10 mg/mL solution|0.3 g^pecan|1^pecan|10^pecans|^|^|^"
		case age=="<4" and stdExt=="ext"
			doses="1 mL^10 mg/mL solution|2.5 mL^10 mg/mL solution|0.1 g^pecan|0.3 g^pecan|1^pecan|4^pecans|^|^"
		case age=="4+" and stdExt=="ext"
			doses="1 mL^10 mg/mL solution|2.5 mL^10 mg/mL solution|0.1 g^pecan|0.3 g^pecan|1^pecan|4^pecans|6^pecans|^"
	endcond
	DOCUMENT.FOODALLERGEN="pecan"
	DOCUMENT.CHALLENGE_FOOD=challengeFood
	//Call function below to update fields on screen:
	fnFoodChallengeUpdateFieldsOnScreen("",doses)
	return ""
}
/*******************************************************************/
/* Function: fnFoodChallengePistachio()
/* Purpose: Fill in fields based on pistachio selection and button clicked.
/*******************************************************************/
fn fnFoodChallengePistachio(food,age,stdExt)
{
	local doses="",challengeFood=""
	// Load the doses based on food type, age range, and whether user clicked Standard or Extended 
	if food=="Pistachio" then
		challengeFood="pistachio/pistachio flour solution"
		cond
			case age=="<4" and stdExt=="std"
				doses="5 mL^10 mg/mL solution|1^pistachio|4^pistachios|12^pistachios|^|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="5 mL^10 mg/mL solution|2^pistachio|4^pistachios|20^pistachios|^|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="1 mL^10 mg/mL solution|5 mL^10 mg/mL solution|0.1 g^pistachio|0.35 g^pistachio|1^pistachio|4^pistachios|12^pistachios|^"
			case age=="4+" and stdExt=="ext"
				doses="1 mL^10 mg/mL solution|5 mL^10 mg/mL solution|0.1 g^pistachio|0.35 g^pistachio|1^pistachio|4^pistachios|20^pistachios|^"
		endcond
	endif
	if food=="Pistachio Butter" then
		challengeFood="pistachio solution/pistachio butter/pistachios"
		cond
			case age=="<4" and stdExt=="std"
				doses="5 mL^10 mg/mL solution|0.7 g^pistachio butter|2.6 g^pistachio butter|7.7 g^pistachio butter|^|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="5 mL^10 mg/mL solution|1.3 g^pistachio butter|2.6 g^pistachio butter|13 g^pistachio butter|^|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="1 mL^10 mg/mL solution|5 mL^10 mg/mL solution|0.1 g^pistachio butter|0.3 g^pistachio butter|0.6 g^pistachio butter|2.6 g^pistachio butter|7.7 g^pistachio butter|^"
			case age=="4+" and stdExt=="ext"
				doses="1 mL^10 mg/mL solution|5 mL^10 mg/mL solution|0.1 g^pistachio butter|0.3 g^pistachio butter|0.6 g^pistachio butter|2.6 g^pistachio butter|13 g^pistachio butter|^"
		endcond
	endif
	if doses=="" then
		userok("Can't find the dose")
		return ""
	endif
	DOCUMENT.FOODALLERGEN="pistachio"
	DOCUMENT.CHALLENGE_FOOD=challengeFood
	//Call function below to update fields on screen:
	fnFoodChallengeUpdateFieldsOnScreen("",doses)
	return ""
}
/*******************************************************************/
/* Function: fnFoodChallengeSesame()
/* Purpose: Fill in fields based on sesame selection and button clicked.
/*******************************************************************/
fn fnFoodChallengeSesame(food,age,stdExt)
{
	local doses="",challengeFood=""
	// Load the doses based on food type, age range, and whether user clicked Standard or Extended 
	if food=="Sesame" then
		challengeFood="sesame solution/sesame seed"
		cond
			case age=="<4" and stdExt=="std"
				doses="8 mL^5 mg/mL sesame solution|0.6 g^unhulled sesame seeds|4.5 g^unhulled sesame seeds|6.4 g^unhulled sesame seeds|^|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="8 mL^5 mg/mL sesame solution|0.6 g^unhulled sesame seeds|6.4 g^unhulled sesame seeds|9.3 g^unhulled sesame seeds|^|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="1 mL^5 mg/mL sesame solution|8 mL^5 mg/mL sesame solution|0.2 g^unhulled sesame seeds|1.1 g^unhulled sesame seeds|3.5 g^unhulled sesame seeds|6.4 g^unhulled sesame seeds|^|^"
			case age=="4+" and stdExt=="ext"
				doses="1 mL^5 mg/mL sesame solution|8 mL^5 mg/mL sesame solution|0.2 g^unhulled sesame seeds|1.1 g^unhulled sesame seeds|3.5 g^unhulled sesame seeds|4.5 g^unhulled sesame seeds|6.4 g^unhulled sesame seeds|^"
		endcond
	endif
	if food=="Tahini" then
		challengeFood="sesame solution/tahini"
		cond
			case age=="<4" and stdExt=="std"
				doses="8 mL^5 mg/mL sesame solution|650 mg^tahini|4502 mg^tahini|6401 mg^tahini|^|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="8 mL^5 mg/mL sesame solution|650 mg^tahini|6401 mg^tahini|9302 mg^tahini|^|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="1 mL^5 mg/mL solution|8 mL^5 mg/mL solution|250 mg^tahini|1120 mg^tahini|3500 mg^tahini|6401 mg^tahini|^|^"
			case age=="4+" and stdExt=="ext"
				doses="1 mL^5 mg/mL solution|8 mL^5 mg/mL solution|250 mg^tahini|1120 mg^tahini|3500 mg^tahini|4502 mg^tahini|6401 mg^tahini|^"
		endcond
	endif
	if doses=="" then
		userok("Can't find the dose")
		return ""
	endif
	DOCUMENT.FOODALLERGEN="sesame"
	DOCUMENT.CHALLENGE_FOOD=challengeFood
	//Call function below to update fields on screen:
	fnFoodChallengeUpdateFieldsOnScreen("",doses)
	return ""
}
/*******************************************************************/
/* Function: fnFoodChallengeSoy()
/* Purpose: Fill in fields based on the Soy button clicked.
/*******************************************************************/
fn fnFoodChallengeSoy(age,stdExt)
{
	local doses=""
	cond
		case age=="<4" and stdExt=="std"
			doses="0.3 mL^soy milk|3 mL^soy milk|30 mL^soy milk|120 mL^soy milk|^|^|^|^"
		case age=="4+" and stdExt=="std"
			doses="0.3 mL^soy milk|3 mL^soy milk|30 mL^soy milk|210 mL^soy milk|^|^|^|^"
		case age=="<4" and stdExt=="ext"
			doses="2 mL^1 mg/mL solution|6 mL^1 mg/mL solution|0.3 mL^soy milk|1 mL^soy milk|3 mL^soy milk|10 mL^soy milk|45 mL^soy milk|90 mL^soy milk"
		case age=="4+" and stdExt=="ext"
			doses="2 mL^1 mg/mL solution|6 mL^1 mg/mL solution|0.3 mL^soy milk|1 mL^soy milk|3 mL^soy milk|10 mL^soy milk|60 mL^soy milk|180 mL^soy milk"
	endcond
	DOCUMENT.FOODALLERGEN="soy"
	DOCUMENT.CHALLENGE_FOOD="soy milk"
	//Call function below to update fields on screen:
	fnFoodChallengeUpdateFieldsOnScreen("",doses)
	return ""
}
/*******************************************************************/
/* Function: fnFoodChallengeWalnut()
/* Purpose: Fill in fields based on walnut selection and button clicked.
/*******************************************************************/
fn fnFoodChallengeWalnut(food,age,stdExt)
{
	local doses="",challengeFood=""
	// Load the doses based on food type, age range, and whether user clicked Standard or Extended 
	if food=="Walnut" then
		challengeFood="walnut/walnut flour solution"
		cond
			case age=="<4" and stdExt=="std"
				doses="1 mL^5 mg/mL solution|8 mL^5 mg/mL solution|0.3 g^walnut|1^walnut|5^whole walnuts|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="1 mL^5 mg/mL solution|8 mL^5 mg/mL solution|0.3 g^walnut|1^walnut|9^whole walnuts|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="1 mL^5 mg/mL solution|8 mL^5 mg/mL solution|0.1 g^walnut|0.3 g^walnut|1/2^walnut|2 1/2^walnuts|3^whole walnuts|^"
			case age=="4+" and stdExt=="ext"
				doses="1 mL^5 mg/mL solution|8 mL^5 mg/mL solution|0.1 g^walnut|0.3 g^walnut|1/2^walnut|3^whole walnuts|6^whole walnuts|^"
		endcond
	endif
	if food=="Walnut Milk" then
		challengeFood="walnut flour solution/walnut milk"
		cond
			case age=="<4" and stdExt=="std"
				doses="1 mL^5 mg/mL solution|0.5 mL^walnut milk|4 mL^walnut milk|49 mL^walnut milk|245 mL^walnut milk|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="1 mL^5 mg/mL solution|0.5 mL^walnut milk|4 mL^walnut milk|49 mL^walnut milk|441 mL^walnut milk|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="1 mL^5 mg/mL solution|0.5 mL^walnut milk|1 mL^walnut milk|4 mL^walnut milk|24 mL^walnut milk|122 mL^walnut milk|146 mL^walnut milk|^"
			case age=="4+" and stdExt=="ext"
				doses="1 mL^5 mg/mL solution|0.5 mL^walnut milk|1 mL^walnut milk|4 mL^walnut milk|24 mL^walnut milk|146 mL^walnut milk|292 mL^walnut milk|^"
		endcond
	endif
	if food=="Walnut Butter" then
		challengeFood="walnut solution/walnut butter/walnuts"
		cond
			case age=="<4" and stdExt=="std"
				doses="1 mL^5 mg/mL solution|8 mL^5 mg/mL solution|0.3 g^walnut butter|3.9 g^walnut butter|19.5 g^walnut butter|^|^|^"
			case age=="4+" and stdExt=="std"
				doses="1 mL^5 mg/mL solution|8 mL^5 mg/mL solution|0.3 g^walnut butter|3.9 g^walnut butter|35.1 g^walnut butter|^|^|^"
			case age=="<4" and stdExt=="ext"
				doses="1 mL^5 mg/mL solution|8 mL^5 mg/mL solution|0.1 g^walnut butter|0.3 g^walnut butter|1.9 g^walnut butter|9.7 g^walnut butter|11.7 g^walnut butter|^"
			case age=="4+" and stdExt=="ext"
				doses="1 mL^5 mg/mL solution|8 mL^5 mg/mL solution|0.1 g^walnut butter|0.3 g^walnut butter|1.9 g^walnut butter|11.7 g^walnut butter|23.4 g^walnut butter|^"
		endcond
	endif
	if doses=="" then
		userok("Can't find the dose")
		return ""
	endif
	DOCUMENT.FOODALLERGEN="walnut"
	DOCUMENT.CHALLENGE_FOOD=challengeFood
	//Call function below to update fields on screen:
	fnFoodChallengeUpdateFieldsOnScreen("",doses)
	return ""
}


/*******************************************************************/
/* Function: fnFoodChallengeUpdateFieldsOnScreen()
/* Purpose: Fill in fields based on passed in list.
/*******************************************************************/
fn fnFoodChallengeUpdateFieldsOnScreen(postFix,doses)
{
	//Update common fields:
	eval("DOCUMENT.HEP_LOCK"+postFix+"='no'")
	eval("DOCUMENT.CHALLENGE_TYPE"+postFix+"='open'")
	eval("DOCUMENT.DOSING_INTER"+postFix+"='20 min'")
	eval("DOCUMENT.OBSERVATION"+postFix+"='90 min'")
	//Create an array from doses, then loop through and update fields.
	//If array element is empty, the eval statements will clear those fields.
	local i,arr=getfield(doses,"|","")
	for i=1,i<=8,i=i+1
	do
		local thisRow=getfield(arr[i],"^","")
		eval("DOCUMENT.FCR_DOSE"+i+"_AMT"+postFix+"='"+thisRow[1]+"'")
		eval("DOCUMENT.FCR_DOSE"+i+"_CONC"+postFix+"='"+thisRow[2]+"'")
		local temp=""
		if match(thisRow[2],"solution")>0 then
			temp=thisRow[1]+" of "+thisRow[2]
		else 
			//Only add text to temp if we have a non-empty array element:
			if thisRow[1]<>"" then
				temp=thisRow[1]+" "+thisRow[2]
			endif
		endif
		eval("DOCUMENT.FCR_GIVEN"+i+"='"+temp+"'")
	endfor
	return ""
}



/*******************************************************************/
/* Function: fnCalculateElapsedTime
/* Purpose: Given a start time and finish time, calculate interval.
/*******************************************************************/
fn fnCalculateElapsedTime(strStart, strFinish)
{
local lStart
local lFinish
local lTime
local temp=""
/* remove all non-numeric chars from time (except ":") */
local i
for i=1,i<=size(strStart),i=i+1
do
	if ((strStart[i]>="0" and strStart[i]<="9")or strStart[i]=":") then
		temp=temp+strStart[i]
	endif
endfor
strStart=temp
temp=""
for i=1,i<=size(strFinish),i=i+1
do
	if ((strFinish[i]>="0" and strFinish[i]<="9")or strFinish[i]=":") then
		temp=temp+strFinish[i]
	endif
endfor
strFinish=temp
/* Convert times into arrays with hours and minutes */
lStart = getfield(strStart, ":")
lFinish = getfield(strFinish, ":")

/* Convert strings to values */
lStart[1] = val(lStart[1])
lStart[2] = val(lStart[2])
lFinish[1] = val(lFinish[1])
lFinish[2] = val(lFinish[2])

/* if finish hour < starting hour, must have crossed 12:00 noon, so add 12 to finish hour */
if (lFinish[1]<lStart[1]) then
	lFinish[1]=lFinish[1]+12
endif

/* If minutes for finish time less than start time add 60 minute and subtract 1 hour to facilitate calculation */
if (lFinish[2] < lStart[2]) then
    lFinish[1] = lFinish[1] - 1
    lFinish[2] = lFinish[2] + 60
endif

/* Subtract hours and minutes and create string */
temp=str(lFinish[2] - lStart[2])
if (size(temp)==1) then
	temp="0"+temp
endif
lTime = str(lFinish[1] - lStart[1]) + ":" + temp
return (lTime)
}
